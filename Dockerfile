FROM openjdk:8
ADD target/springboot-0.0.1-SNAPSHOT.jar springboot-0.0.1-SNAPSHOT.jar
EXPOSE 9112
ENTRYPOINT ["java","-jar","springboot-0.0.1-SNAPSHOT.jar"]